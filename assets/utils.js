import Cookies from 'js-cookie'
import config from '../nuxt.config'

export const TOKEN_KEY = 'token'
export const USER_KEY = 'user'

export const setToken = (token) => {
  Cookies.set(TOKEN_KEY, token, { expires: 1, domain: config.env.domain })
}

export const getToken = () => {
  return Cookies.get(TOKEN_KEY)
}

export const setUser = (user) => {
  Cookies.set(USER_KEY, user, { domain: config.env.domain })
}

export const getUser = () => {
  return Cookies.get(USER_KEY)
}

export function parseTime (time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if ((typeof time === 'string') && (/^[0-9]+$/.test(time))) {
      time = parseInt(time)
    }
    if ((typeof time === 'number') && (time.toString().length === 10)) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  return format.replace(/{([ymdhisa])+}/g, (result, key) => {
    let value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') {
      return ['日', '一', '二', '三', '四', '五', '六'][value]
    }
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
}

let timeout = null
export const debounce = (fn, wait) => {
  if (timeout !== null) { clearTimeout(timeout) }
  timeout = setTimeout(fn, wait)
}

export const formatSize = (size) => {
  if (size > (1024 * 1024)) {
    return (size / (1024 * 1024)).toFixed(2) + 'MB'
  } else if (size > 1024) {
    return (size / 1024).toFixed(0) + 'KB'
  } else {
    return size + 'B'
  }
}

export const CSDN_URL_PATTERN = /^(http(s)?:\/\/download\.csdn\.net\/(download|detail)\/).+\/\d+(\?.+)?$/
export const WENKU_URL_PATTERN = /^(http(s)?:\/\/w(en)?k(u)?\.baidu\.com\/view\/).+$/
export const DOCER_URL_PATTERN = /^(http(s)?:\/\/www\.docer\.com\/(webmall\/)?preview\/).+$/
export const ZHIWANG_URL_PATTERN = /^(http(s)?:\/\/kns(8)?\.cnki\.net\/KCMS\/detail\/).+$/
export const QIANTU_URL_PATTERN = /^(http(s)?:\/\/www\.58pic\.com\/newpic\/)\d+(\.html)$/
export const ITEYE_URL_PATTERN = /^http(s)?:\/\/www\.iteye\.com\/resource\/.+-\d+$/
export const MBZJ_URL_PATTERN = /^(http(s)?:\/\/www\.cssmoban\.com\/(cssthemes|wpthemes)\/\d+\.shtml).*$/
