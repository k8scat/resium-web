# resium-ui

基础镜像: https://github.com/hsowan-me/resium-ui-base-image

## Todo

* [ ] SEO**********very important
* [ ] 注册码
* [ ] GitHub
* [ ] SSR
* [ ] user index component
* [ ] 移动端
* [ ] qq注册登录

## 生成二维码

https://www.jianshu.com/p/496fd1cbee8d

## js uuid

https://github.com/uuidjs/uuid

## js replace

const cid = uuidv1().replace(/-/g, '')

https://blog.csdn.net/zhouxukun123/article/details/80413906

## 存储

* [七牛云存储](https://portal.qiniu.com/kodo/bucket/resource?bucketName=ncucoder)

## References

* [cross-env](https://github.com/kentcdodds/cross-env)
* [Vue.js 2.0 Render 函数](https://www.w3cschool.cn/vuejs2/render-function.html)
* [browser-md5-file](https://github.com/forsigner/browser-md5-file)

## 公众号文章

* [ ] @click 有一个event参数，https://www.jianshu.com/p/b078cfe97c92
* [ ] 正则表达式
* [ ] 爬虫必备技能xpath
* [ ] 使用jenkins实现多节点同步搭建后端集群
* [ ] nginx实现负载均衡和高可用

## 使用crypto-js获取文件的hash值，从而检验文件的完整性
