FROM node:12-alpine
LABEL maintainer="hsowan <hsowan.me@gmail.com>"
EXPOSE 3000
WORKDIR /data/resium-web
COPY . .
RUN npm install && \
    npm install -g serve && \
    npm run build
CMD serve -s dist -l tcp://0.0.0.0:3000
