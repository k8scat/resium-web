export default function ({ $axios, store, redirect }) {
  // If the user is not authenticated
  if (!store.state.token) {
    store.dispatch('handleLogout')
    return redirect('/')
  }

  if (!store.state.user) {
    $axios.$get('get_user/').then((data) => {
      if (data.code === 200) {
        store.commit('setUser', data.user)
      } else {
        store.dispatch('handleLogout')
        return redirect('/')
      }
    })
  }
}
