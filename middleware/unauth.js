export default function ({ $axios, store, redirect }) {
  // If the user is authenticated
  if (store.state.token) {
    if (store.state.user) {
      return redirect('/')
    } else {
      $axios.$get('get_user/').then((data) => {
        if (data.code === 200) {
          store.commit('setUser', data.user)
          return redirect('/')
        } else {
          store.dispatch('handleLogout')
        }
      })
    }
  }
}
