module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  rules: {
    "vue/html-self-closing": ["error",{
      "html": {
        "void": "always",
        "normal": "any",
        "component": "any"
      },
      "svg": "always",
      "math": "always"
    }]
  }
}
