import { getToken, setToken, parseTime, getUser, setUser } from '~/assets/utils'

export const state = () => {
  return {
    token: getToken(),
    user: getUser() ? JSON.parse(getUser()) : ''
  }
}

export const mutations = {
  setToken (state, token) {
    state.token = token
    setToken(token)
  },
  setUser (state, user) {
    if (user.create_time) {
      user.create_time = parseTime(user.create_time, '{y}-{m}-{d}')
    }
    state.user = user
    setUser(user)
  }
}

export const actions = {
  handleLogin ({ commit }, { token, user }) {
    commit('setToken', token)
    commit('setUser', user)
  },
  handleLogout ({ commit }) {
    commit('setToken', '')
    commit('setUser', '')
  }
}
