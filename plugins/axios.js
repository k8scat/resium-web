import { Message } from 'iview'

export default function ({ $axios, redirect, store }, inject) {
  $axios.setBaseURL(process.env.BASE_URL)

  $axios.onRequest((config) => {
    // console.log('Making request to ' + config.url)
    // console.log(config.headers)
    const token = store.state.token
    if (token) {
      config.headers.common.Authorization = 'Bearer ' + token
    }
  })

  $axios.onResponse((response) => {
    if (response.data.code === 401) {
      store.dispatch('handleLogout')
      return redirect('/')
    }
  })

  $axios.onError((error) => {
    if (error && error.response) {
      const res = error.response
      if (res.status === 403) {
        Message.error({
          content: '请求频率过快，请稍后再试',
          duration: 3
        })
      }
    } else {
      Message.error('请求失败')
    }
    return Promise.reject(error)
  })
}

/* https://axios.nuxtjs.org/ */
