export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: '源自下载',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: '在线资源下载平台'
      },
      {
        name: 'referrer',
        content: 'no-referrer'
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: []
  },
  /*
  ** Customize the progress-bar color
  */
  // loading: { color: '#fff' },
  loading: false,
  /*
  ** Global CSS
  */
  css: [
    'iview/dist/styles/iview.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/iview',
    '~/plugins/axios'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    ['@nuxtjs/dotenv', {
      filename: process.env.NODE_ENV === 'development' ? '.env.development' : '.env.production'
    }]
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/sentry'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    },
    loaders: {
      less: {
        javascriptEnabled: true
      }
    }
  },
  env: {
    dev: process.env.NODE_ENV === 'development',
    ui: process.env.UI,
    baseUrl: process.env.BASE_URL,
    domain: process.env.DOMAIN
  },
  router: {
    middleware: []
  },
  sentry: {
    dsn: 'https://b513d2a10aae490baacb6e8791cee674@o371841.ingest.sentry.io/6038762', // Enter your project's DSN here
    // Additional Module Options go here
    // https://sentry.nuxtjs.org/sentry/options
    config: {
      // Add native Sentry config here
      // https://docs.sentry.io/platforms/javascript/guides/vue/configuration/options/
      environment: process.env.NODE_ENV
    },
    tracing: {
      tracesSampleRate: 1.0,
      vueOptions: {
        tracing: true,
        tracingOptions: {
          hooks: [ 'mount', 'update' ],
          timeout: 2000,
          trackComponents: true
        }
      },
      browserOptions: {}
    }
  }
}
